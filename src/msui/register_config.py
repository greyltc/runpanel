#!/usr/bin/env python3

import argparse
from typing import Sequence, cast
import yaml
import sys
import os
import json
from functools import reduce
import importlib.resources
import redis
import pathlib
import glob
from copy import deepcopy

# from redis_annex import uadd


class ConfigRegisterer(object):
    """Interface for registering a type A configuration"""

    dump: bool = False
    mem_db_url: str = "redis://"
    mem_db_url_env_name: str = "MEM_DB_URL"
    default_config_file_name: str = "system_config.yaml"
    default_aux_glob = pathlib.Path.home().glob("measurement_config.d/*.yaml")  # TODO: should switch to appdirs for this
    conf_paths: list[pathlib.Path] = [pathlib.Path(default_config_file_name)]
    conf_path_base_env_name = "BASE_CONF"
    conf_path_aux_env_name = "AUX_CONFS"
    return_code: int = -1
    idx: str | None = None
    configs: list[dict] = []
    conf_a: None | dict = None  # the final merged config dict
    note: str | None = None

    def __init__(self, mem_db_url: None | str = None, conf_paths: None | list[pathlib.Path] = None):
        if mem_db_url:
            self.mem_db_url = mem_db_url
        elif self.mem_db_url_env_name in os.environ:
            self.mem_db_url = os.environ[self.mem_db_url_env_name]

        paths_from_init = False
        if conf_paths:
            self.conf_paths = conf_paths
            paths_from_init = True
        elif self.conf_path_base_env_name in os.environ:
            self.conf_paths = [pathlib.Path(os.environ[self.conf_path_base_env_name])]
        elif not pathlib.Path(self.conf_paths[0].__fspath__()).exists():
            # nobody told us what configs to use and the default name isnt a file in cwd
            try:
                self.conf_paths = [cast(pathlib.Path, importlib.resources.files(__package__) / self.default_config_file_name)]
            except:
                self.conf_paths = []

        if (not paths_from_init) and (self.conf_path_aux_env_name in os.environ):
            globs = os.environ[self.conf_path_aux_env_name].split(":")
            for aglob in globs:
                for match in glob.glob(aglob):
                    self.conf_paths.append(pathlib.Path(match))
        else:
            for match in self.default_aux_glob:
                self.conf_paths.append(pathlib.Path(match))

        # alnum sort except first item
        try:
            base_conf = self.conf_paths.pop(0)
        except Exception as e:
            raise ValueError("Could not find a configuration file")

        # now we want to prefix config file names with a 2 digit number like 50_conf.yaml
        # then files with higher number prefixes will win key conflict battles against lower ones
        self.conf_paths.sort(key=lambda x: x.name.lower())

        self.conf_paths = [base_conf] + self.conf_paths
        self.return_code = -2

    @staticmethod
    # deep merges 2 dictionaries through recursion.
    # key conflict battles get won by dict y
    # if concat_layout_lists is true, then matching lists with key "layouts" will be concatenated instead of overwritten
    def dict_of_dicts_merge(x, y, concat_layout_lists:bool=False):
        z = {}
        overlapping_keys = x.keys() & y.keys()
        for key in overlapping_keys:
            if isinstance(x[key], dict) and isinstance(y[key], dict):
                z[key] = ConfigRegisterer.dict_of_dicts_merge(x[key], y[key], concat_layout_lists=concat_layout_lists)
            elif isinstance(x[key], list) and isinstance(y[key], list) and concat_layout_lists and (key == "layouts"):
                z[key] = deepcopy(x[key]) + deepcopy(y[key])
            else:
                z[key] = deepcopy(y[key])
        for key in x.keys() - overlapping_keys:
            z[key] = deepcopy(x[key])
        for key in y.keys() - overlapping_keys:
            z[key] = deepcopy(y[key])
        return z

    def run(self) -> int:
        """main program logic"""
        if self.dump:
            with redis.Redis.from_url(self.mem_db_url) as r:
                for idx, entry in r.xrevrange("conf_as", count=self.dump):
                    self.configs.append({idx.decode(): entry})

            self.return_code = 0
        else:
            for path in self.conf_paths:
                assert path.exists(), "Config file does not exist."
            # parse all the config files
            for path in self.conf_paths:
                with open(path, "r") as fh:
                    # self.configs.append(yaml.load(fh, Loader=yaml.FullLoader))
                    self.configs.append(yaml.safe_load(fh))

            for i, d in enumerate(self.configs):
                if i == 0:
                    merged_conf = d
                else:
                    merged_conf = ConfigRegisterer.dict_of_dicts_merge(merged_conf, d, concat_layout_lists=False)

            # remove possible layout dupes (in favor of later ones) possibly caused by concatenating layout lists
            name_memory = {}
            for i, l in enumerate(merged_conf["substrates"]["layouts"]):
                layout_name = l["name"]
                if layout_name in name_memory:
                    del merged_conf["substrates"]["layouts"][name_memory[layout_name]]
                name_memory[layout_name] = i

            self.conf_a = merged_conf
            if self.conf_a:
                with redis.Redis.from_url(self.mem_db_url) as r:
                    try:
                        self.idx = r.xadd("conf_as", fields={"json": json.dumps(self.conf_a), "notes": str(self.note)}, maxlen=100, approximate=True).decode()
                        self.return_code = 0
                    except:
                        self.return_code = -1

        return self.return_code


def cli(cli_args: Sequence[str] | None = None, program: str | None = None) -> int:
    # argparse setup
    parser = argparse.ArgumentParser(description="Registers a configuration file with the database")
    if program:
        parser.prog = program
    parser.epilog = f'example usage: {parser.prog} --note="good config!" --conf-file $(python -c "import site; print(site.getsitepackages()[0])")/msui/system_config.yaml ~/measurement_config.d/*.yaml'

    parser.add_argument("--mem-db-url", help="Memory database connection string")
    parser.add_argument("--conf-file", metavar="conf_file", nargs="*", help="YAML formatted configuration file(s)")
    parser.add_argument("--note", type=str, default=None, help="Optional note to attach to this config")
    parser.add_argument("--dump", type=int, default=0, help="Dump this many historical configs to files (existing files may be overwritten)")

    if cli_args:
        args = parser.parse_args(cli_args)
    else:
        args = parser.parse_args()

    cr_init_args = {}
    if args.mem_db_url:
        cr_init_args["mem_db_url"] = args.mem_db_url
    if args.conf_file:
        conf_paths = []
        for conf in args.conf_file:
            conf_paths.append(pathlib.Path(conf).expanduser())
        cr_init_args["conf_paths"] = conf_paths

    cr = ConfigRegisterer(**cr_init_args)  # initialize
    cr.dump = args.dump
    cr.note = args.note
    ret_code = cr.run()  # main program logic
    if cr.dump:
        for dconf in cr.configs:
            id = next(iter(dconf))
            with open(f"{id}.yaml", "w") as yc:
                print(f'Dumping {yc.name} with note: "{dconf[id][b"notes"].decode()}"')
                yaml.safe_dump(json.loads(dconf[id][b"json"]), yc, default_flow_style=False)
    else:
        if cr.idx:
            print(cr.idx)
    return ret_code


if __name__ == "__main__":
    # sys.exit(cli())
    sys.exit(cli(["--conf-file", str(importlib.resources.files("msui") / ConfigRegisterer.default_config_file_name), "~/measurement_config.d/ovr.yaml", "~/measurement_config.d/do_virt.yaml"]))
    # sys.exit(cli(["--dump", "2"]))

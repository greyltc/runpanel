import gi

# gi.require_versions({"GLib": "2.0", "Gio": "2.0"})

from gi.repository import GLib, Gio


class Sysdbus(object):
    bus_name = None
    ready = False

    def __init__(self, bus_name="org.freedesktop.systemd1"):
        self.bus_name = bus_name

    def connect(self, bus_type=Gio.BusType.SESSION):
        """forms dbus connection"""
        bus_name_path = "/" + self.bus_name.replace(".", "/")

        try:
            bus_get_setup = {}
            bus_get_setup["bus_type"] = bus_type
            bus_get_setup["cancellable"] = None
            session_bus_connection = Gio.bus_get_sync(**bus_get_setup)

            # set up proxy to systemd Manager
            srv_prx_setup = {}
            srv_prx_setup["connection"] = session_bus_connection
            srv_prx_setup["flags"] = Gio.DBusProxyFlags.DO_NOT_AUTO_START
            srv_prx_setup["info"] = None
            srv_prx_setup["name"] = self.bus_name
            srv_prx_setup["object_path"] = bus_name_path
            srv_prx_setup["interface_name"] = self.bus_name + ".Manager"
            srv_prx_setup["cancellable"] = None
            self.sysdmgr = Gio.DBusProxy.new_sync(**srv_prx_setup)

            self.ready = True
        except Exception:
            self.ready = False

    # calls a function over dbus
    def db_call(self, proxy, method_name, parameters=None, flags=Gio.DBusCallFlags.NO_AUTO_START, timeout_msec=500, cancellable=None):
        call_setup = {}
        call_setup["method_name"] = method_name
        call_setup["parameters"] = self.to_variant(parameters)
        call_setup["flags"] = flags
        call_setup["timeout_msec"] = timeout_msec
        call_setup["cancellable"] = cancellable
        return proxy.call_sync(**call_setup)

    # takes a list of python objects and tries to make them into a GLib Variant
    def to_variant(self, things):
        if things is None:
            return None
        else:
            types = ""
            for thing in things:
                if isinstance(thing, str):
                    types = types + "s"
                elif isinstance(thing, int):
                    types = types + "i"
                elif isinstance(thing, float):
                    types = types + "d"
                elif isinstance(thing, bool):
                    types = types + "b"
                else:
                    print(f"DANGER: I can't gVariant {thing} of type {type(thing)}")
            return GLib.Variant(f"({types})", things)

    def start_service(self, service):
        job = self.db_call(self.sysdmgr, "StartUnit", (service, "replace"))
        return job.unpack()[0]

    def stop_service(self, service):
        job = self.db_call(self.sysdmgr, "StopUnit", (service, "replace"))
        return job.unpack()[0]

    def restart_service(self, service):
        job = self.db_call(self.sysdmgr, "RestartUnit", (service, "replace"))
        return job.unpack()[0]

    def list_units(self):
        units = self.db_call(self.sysdmgr, "ListUnits")
        return units.unpack()[0]

    def check_sub_state(self, unit_name, substate_target="running"):
        """returns true if the unit's sub-state is substate_target"""
        matches_target = False
        unitlines = self.list_units()
        for unitline in unitlines:
            if unitline[0] == unit_name:
                if unitline[4] == substate_target:
                    matches_target = True
                break
        return matches_target

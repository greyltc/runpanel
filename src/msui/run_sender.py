#!/usr/bin/env python3

import argparse
import json
import pathlib
import sys
import paho.mqtt.client as mqttc
import uuid


class RunSender(object):
    """Command line interface that spoofs a user pressing the run button in runpanel"""

    parser: argparse.ArgumentParser
    server_host: str
    run_file: pathlib.Path
    slots_file: pathlib.Path
    run_started: bool
    return_code: int
    run_msg: dict

    def __init__(self):
        default_server_host = "127.0.0.1:1883"
        default_run_file = pathlib.Path("run.gsf.json")
        self.run_started = False
        self.return_code = 1
        self.run_msg = {}

        self.server_host = default_server_host
        self.run_file = default_run_file

        # argparse setup
        self.parser = argparse.ArgumentParser(
            prog="runsender",
            description="Sends a run command to the backend server just like the GUI does. Returns when the run is complete",
            epilog='example usage: runsender --server-host="127.0.0.1:1883" --run-file="run.gsf.json" --slots-file="slots.tsv"',
        )
        self.parser.add_argument("--server-host", default=default_server_host, help="host[:port] to send the run command to")
        self.parser.add_argument("--run-file", default=default_run_file, type=pathlib.Path, help="json formatted text file to load the run setup info from")
        self.parser.add_argument("--slots-file", type=pathlib.Path, help="tsv formatted text file to load the slot/substrate info from")

    def parse_args(self):
        """parses command line arguments"""
        args = self.parser.parse_args()
        self.server_host = args.server_host
        self.run_file = args.run_file
        self.slots_file = args.slots_file

    def on_connect(self, client: mqttc.Client, userdata, flags, rc):
        """acts on a new connection"""
        client.subscribe("measurement/status/#", qos=2)
        client.subscribe("measurement/log/#", qos=2)

    def on_message(self, client: mqttc.Client, userdata, msg):
        """acts on a new message"""
        try:
            m = json.loads(msg.payload)
        except:
            pass  # message parse failure, likely no problem, not all message formats may be supported here
        else:  # examine by message topic
            if (m is not None) and hasattr(msg, "topic"):
                if msg.topic == "measurement/status":
                    self.run_handler_status = m
                    if m == "Ready":
                        if not self.run_started:
                            self.run_started = True
                            client.publish("measurement/run", json.dumps(self.run_msg), qos=2)
                        else:
                            self.return_code = 4  # server desync A: got a ready message when we were expecting a complete one
                            client.disconnect()
                    elif (m == "Busy") and (not self.run_started):
                        self.return_code = 2  # server not ready to start run
                        client.disconnect()
                elif msg.topic == "measurement/log":
                    if m["msg"] == "Run complete!":
                        if not self.run_started:
                            self.return_code = 3  # server desync B: got a complete message before we started the task
                        else:
                            self.return_code = 0  # success
                        client.disconnect()

    def run(self) -> int:
        """main program logic"""
        try:  # load the run file
            with open(str(self.run_file), "r") as f:
                loaded = json.load(f)
                self.run_msg["rundata"] = loaded["rundata"]
                self.run_msg["uuid"] = f"0x{uuid.uuid4().hex}"
        except Exception as e:
            raise ValueError(f"Could not parse {self.run_file}: {e}")

        if self.slots_file is not None:
            try:  # load the slots file
                with open(str(self.slots_file), "r") as f:
                    slotblock = f.read()
                lines = slotblock.splitlines()  # discards ends
                dlm = "\t"
                self.run_msg["slots"] = [line.split(dlm) for line in lines]
            except Exception as e:
                raise ValueError(f"Could not parse {self.run_file}: {e}")

        # start up the client
        try:
            client_id = f"direct-sender.{uuid.uuid4().hex}"
            client = mqttc.Client(client_id=client_id)
            client.on_connect = self.on_connect
            client.on_message = self.on_message
            hostport = self.server_host.split(":")
            hostport_len = len(hostport)
            if hostport_len == 1:
                client.connect(hostport[0])
            elif hostport_len == 2:
                client.connect(hostport[0], port=int(hostport[1]))
            else:
                raise ValueError("Malformed --server-host argument")

            client.loop_forever()

        except Exception as e:
            raise ValueError(f"Failed to start up the client: {e}")

        return self.return_code


def main():
    rs = RunSender()  # initialize
    rs.parse_args()  # parse command line argumants
    return rs.run()  # main program logic


if __name__ == "__main__":
    sys.exit(main())

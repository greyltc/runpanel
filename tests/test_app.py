import unittest
import asyncio
import struct
import socket
import threading
import pickle
import logging
import sys
import tempfile
from msui.gui import App


class MulticastRXTestProtocol:
    def __init__(self, output, on_con_lost, goal):
        self.output = output
        self.on_con_lost = on_con_lost
        self.transport = None
        self.goal = goal
        self.lg = logging.getLogger(__name__)

    def connection_made(self, transport):
        self.transport = transport
        # mreq = struct.pack("4sl", socket.inet_aton(transport._address[0]), socket.INADDR_ANY)
        # transport._sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    def datagram_received(self, data, addr):
        channel = None
        payload_unpacked = None
        try:
            # TODO: consider hmac to prevent tampering
            message = pickle.loads(data)
            payload = message["payload"]
            if "channel" in message:
                channel = message["channel"]
            payload_unpacked = pickle.loads(payload)
        except Exception as e:
            self.lg.debug("Failed to unpickle a datagram's payload")
        if self.goal == "test multicast":
            if isinstance(channel, str):
                self.output.set_result(True)
            else:
                self.output.set_result(False)
            self.transport.close()  # close the connection because the test is over
        elif self.goal == "dump run stuff":
            if channel == "measurement/run":
                self.output.set_result(payload_unpacked)
                self.transport.close()  # close the connection because the test is over
        else:
            self.lg.debug("Goal unrecognized")

    def connection_lost(self, exc):
        if self.on_con_lost.cancelled() == False:
            self.on_con_lost.set_result(True)


class AppTestCase(unittest.TestCase):
    """GUI App testing code"""

    mc_output = "output future result never set!"

    def setUp(self) -> None:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
        self.lg = logging.getLogger(__name__)
        return super().setUp()

    def set_mc_test_result(self, future):
        self.mc_output = future.result()

    async def run_rx_listener(self, remote_addr, goal, timeout):
        loop = asyncio.get_running_loop()
        output = loop.create_future()
        output.add_done_callback(self.set_mc_test_result)
        on_con_lost = loop.create_future()

        # set up the multicast rx socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(remote_addr)
        mreq = struct.pack("4sl", socket.inet_aton(remote_addr[0]), socket.INADDR_ANY)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

        # transport, protocol = await loop.create_datagram_endpoint(lambda: MulticastRXerProtocol(output, on_con_lost), remote_addr=remote_addr, reuse_port=True)
        transport, protocol = await loop.create_datagram_endpoint(lambda: MulticastRXTestProtocol(output, on_con_lost, goal), sock=sock)

        try:
            await asyncio.wait_for(on_con_lost, timeout)  # wait for connection loss or timeout
        except Exception as e:
            self.lg.debug(f"Waited {timeout} seconds, but the connection never closed!")
        finally:
            transport.close()

    def run_asyncio(self, remote_addr, goal, timeout):
        asyncio.run(self.run_rx_listener(remote_addr, goal, timeout))

    def test_init(self):
        """check that we can init"""
        app = App()
        self.assertIsInstance(app, App)

    def test_run(self):
        """just run the whole gui without sys.argv, user interaction required: exit the gui after it launches or wait try_for sec"""
        try_for = 10  # the user must complete the interaction required in this many seconds or else the test is aborted
        app = App(max_runtime=try_for)
        ret_code = app.run()
        self.assertEqual(ret_code, 0)

    def test_multicast(self):
        """check that multicasat is working, user interaction required: exit the gui after it launches or wait try_for sec"""
        try_for = 10  # the user must complete the interaction required in this many seconds or else the test is aborted
        app = App(max_runtime=try_for)
        t = threading.Thread(target=self.run_asyncio, args=((app.multicast_group, app.multiacst_port), "test multicast", try_for + 5))
        t.start()

        app.run()
        t.join()
        self.assertEqual(self.mc_output, True)

    def test_gen_run_dump(self):
        """dump the entire run message, user interaction required: push run button, exit the gui or wait try_for sec"""
        # to run this manually from the top level:
        # PYTHONPATH="src" python -m unittest -v tests.test_app.AppTestCase.test_gen_run_dump
        try_for = 30  # the user must complete the interaction required in this many seconds or else the test is aborted
        app = App(max_runtime=try_for)

        t = threading.Thread(target=self.run_asyncio, args=((app.multicast_group, app.multiacst_port), "dump run stuff", try_for - 1))
        t.start()

        app.run()
        t.join()
        self.assertIsInstance(self.mc_output, dict)
        with tempfile.NamedTemporaryFile(suffix=".p", delete=False) as df:
            pickle.dump(self.mc_output, df)
            msg = f"run details pickled into to {df.name}"
            self.lg.debug(msg)

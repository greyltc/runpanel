import unittest
from msui.register_config import ConfigRegisterer
import textwrap
import io
from importlib import resources
import pathlib


class RunsenderCase(unittest.TestCase):
    """testing for non-gui run initaitor"""

    testing_db_name = "testing"

    def test_init(self):
        """check that we can init"""

        cr = ConfigRegisterer()
        self.assertIsInstance(cr, ConfigRegisterer)

    def test_send(self):
        """tests registering a basic config"""

        yaml_str = """
            # configuration settings not settable in gui

            setup:
              site: "The Moon"  # location/customer
              name: "R&D testbed number two"  # name used to describe the system

            # configuration of configuration
            # relative paths given here are taken to be relative to the user's home directory
            meta:
              # any .yaml files found in this folder will be loaded as if they were inline in this file
              # for duplicated config parameters, those in this directory will take precedence
              include_path: measurement_config.d
            """
        yaml_dent = textwrap.dedent(yaml_str).lstrip()

        cr = ConfigRegisterer()
        cr.connection_string = f"postgresql:///{self.testing_db_name}"
        cr.config_fhs = [io.StringIO(yaml_dent)]
        self.assertGreaterEqual(cr.run(), 1)

    def test_full_send_with_merge(self):
        """tests merging full configs and sending them up"""
        # check that we can get the baseline conf
        with open(pathlib.Path(__file__).parent / "config_stubs" / "system_config.yaml", "r") as fp:
            self.assertTrue(sys_conf := fp.read())
        conf_strs = [sys_conf]
        # now read any aux conf stubs from the home folder
        # TODO: maybe read them from some other place
        uconf_stubs_path = pathlib.Path.home() / "measurement_config.d"
        if uconf_stubs_path.exists():
            for stub in uconf_stubs_path.glob("*.yaml"):
                with open(stub, "r") as fh:
                    conf_strs.append(fh.read())
        cr = ConfigRegisterer()
        cr.connection_string = f"postgresql:///{self.testing_db_name}"
        cr.config_fhs = [io.StringIO(s) for s in conf_strs]
        self.assertGreaterEqual(cr.run(), 1)  # upsert the merged conf dict

    def test_dump(self):
        """tests dumping configs"""

        cr = ConfigRegisterer()
        cr.connection_string = f"postgresql:///{self.testing_db_name}"
        cr.dump = True
        self.assertEqual(cr.run(), 0)
        self.assertGreaterEqual(len(cr.configs), 1)
